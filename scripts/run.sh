#!/bin/bash
# Version: 1.2.0
# Date: 2023-07-30
# Source: todoapp
# Objective:
# Description:
# ChangeLog:

show_help() {
  echo "Usage: $0 [OPTION]"
  echo "Options:"
  echo "  dev     : Run the application using docker-compose.yaml (Development mode)"
  echo "  prod    : Run the application using docker-compose.prod.yaml (Production mode)"
  echo "  dev-api   : Run the application using docker-compose.dev-api.yaml (dev-api mode)"
  echo "  -r, --restart : Restart the docker containers"
  echo "  -h, --help    : Show this help message"
}

if [[ "$1" = "-h" || "$1" = "--help" ]]; then
  show_help
  exit 0
elif [ "$1" = "prod" ]; then
  COMPOSE_FILE="docker-compose.prod.yaml"
  ENVIRONMENT="production"
elif [ "$1" = "dev-api" ]; then
  COMPOSE_FILE="docker-compose.dev-api.yaml"
  ENVIRONMENT="dev-api"
else
  COMPOSE_FILE="docker-compose.yaml"
  ENVIRONMENT="development"
fi

remove_docker_images() {
  echo "Removing docker images"
  docker-compose -f "$COMPOSE_FILE" down --rmi all -v
}

if [[ "$1" = "-r" || "$1" = "--restart" || "$2" = "-r" || "$2" = "--restart" ]]; then
  echo
  echo "1) Optional project restart"
  remove_docker_images
fi

echo
echo "2) Starting docker containers"
docker-compose -f "$COMPOSE_FILE" up --build -d || exit

if [ "$ENVIRONMENT" = "dev-api" ]; then
  echo
  echo "3) Installing python packages"
  docker-compose -f "$COMPOSE_FILE" exec api python -m pip install -r api/requirements.ci.txt

  echo
  echo "4) Running bash in the container"
  docker-compose -f "$COMPOSE_FILE" exec api bash
fi