echo "1) Stop all running containers"
docker stop $(docker ps -qa)

echo "2) Delete all containers"
docker rm $(docker ps -qa)

echo "3) Delete all images"
docker rmi $(docker images -aq) -f

echo "4) Delete all volumes"
docker volume rm $(docker volume ls -q)

echo "5) Delete all networks"
docker network rm $(docker network ls -q)