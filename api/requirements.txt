Flask==2.0.1
werkzeug==2.0.3
Jinja2==3.0.3
gunicorn==20.1.0
Flask-Cors==3.0.8
