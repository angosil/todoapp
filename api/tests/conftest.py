import pytest
from src.app import app


@pytest.fixture
def app_secret_key():
    return "test-secret"


@pytest.fixture(scope="function")
def get_app(app_secret_key):
    app.config['TESTING'] = True
    app.config['SECRET_KEY'] = app_secret_key
    return app


@pytest.fixture
def client(get_app):
    app.config['TESTING'] = True

    with app.test_client() as client:
        with app.app_context():
            pass
        yield client
