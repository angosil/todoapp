def test_welcome(client):
    response = client.get('/')
    assert response.status_code == 200
    json_data = response.get_json()
    assert json_data['message'] == 'Welcome to ToDo rest api v1.0'


def test_get_items(client):
    response = client.get('/items')
    assert response.status_code == 200
    json_data = response.get_json()
    assert json_data['message'] == 'List of items'
    assert json_data['data'] == [
        {
            "name": "Item Name",
            "description": "Item description",
            "completed": False
        },
        {
            "name": "Item Name",
            "description": "Item description",
            "completed": False
        }
    ]


def test_add_item(client):
    item = {
        "name": "Item Name",
        "description": "Item description",
        "completed": False
    }
    response = client.post('/items', json=item)
    assert response.status_code == 200
    json_data = response.get_json()
    assert json_data['message'] == "Item added successfully"
    assert json_data['data'] == item


def test_add_item_bad_request(client):
    response = client.post('/items')
    assert response.status_code == 400
    json_data = response.get_json()
    assert json_data['message'] == "Need parameters"


def test_delete_item(client):
    response = client.delete('/items/0')
    assert response.status_code == 200
    json_data = response.get_json()
    assert json_data['message'] == "Item deleted successfully"


def test_update_item(client):
    item = {
        "name": "Item Name",
        "description": "Item description",
        "completed": False
    }
    response = client.put('/items/0', json=item)
    assert response.status_code == 200
    json_data = response.get_json()
    assert json_data['message'] == "Item updated successfully"
    assert json_data['data'] == item


def test_update_item_bad_request(client):
    response = client.put('/items/0')
    assert response.status_code == 400
    json_data = response.get_json()
    assert json_data['message'] == "Need parameters"
