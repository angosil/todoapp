import logging
from flask import Flask
from flask_cors import CORS
from src.log_setup import setup as log_setup
from src.exception import register_error
from src.routes import routes

app = Flask(__name__)
app.config.from_object("src.config.Config")

# Creating All Objects

# Starting logger
log_setup()

# Starting app
logging.debug("Load cors ...")
CORS(app)
try:
    logging.debug("Register errors ...")
    register_error(app)
    logging.debug("Loading routes ...")
    routes(app)
except Exception as error:
    logging.exception(error)
    raise error

logging.debug("Waiting for calls...")
