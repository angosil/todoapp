import logging
from typing import Dict

from flask import Flask, jsonify, request
from werkzeug.exceptions import BadRequest


class ItemsRoutes:
    app = None

    def __init__(self, app: Flask = None):
        app.add_url_rule('/items', view_func=self.get_items, methods=['GET'])
        app.add_url_rule('/items', view_func=self.add_item, methods=['POST'])
        app.add_url_rule('/items/<item_id>', view_func=self.delete_item, methods=['DELETE'])
        app.add_url_rule('/items/<item_id>', view_func=self.update_item, methods=['PUT'])

    @staticmethod
    def get_items():
        logging.info("Calling /items [GET] endpoint")
        return jsonify(message="List of items", data=[
            {
                "name": "Item Name",
                "description": "Item description",
                "completed": False
            },
            {
                "name": "Item Name",
                "description": "Item description",
                "completed": False
            }
        ])

    @staticmethod
    def add_item():
        logging.info("Calling /items [POST] endpoint")
        body = required(request.get_json(), "Need parameters")
        return jsonify(message="Item added successfully", data=body)

    @staticmethod
    def delete_item(item_id):
        logging.info(f"Calling /items/{item_id} [DELETE] endpoint")
        return jsonify(message="Item deleted successfully")

    @staticmethod
    def update_item(item_id):
        logging.info(f"Calling /items/{item_id} [PUT] endpoint")
        body = required(request.get_json(), "Need parameters")
        return jsonify(message="Item updated successfully", data=body)


def required(param: [Dict, str], message: str) -> Dict | str:
    """
    This function is to raise BadRequest with custom message if some data is None.
    Args:
        param:
        message:

    Returns:
        the data.

    Raises:
            Forbidden: If data is None
    """

    if param is not None:
        return param
    raise BadRequest(message)
