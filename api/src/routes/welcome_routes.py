import logging

from flask import jsonify
from src.config import Config


class WelcomeRoutes:

    def __init__(self, app=None):
        app.add_url_rule('/', view_func=self.welcome, methods=['GET'])

    @staticmethod
    def welcome():
        logging.info("Calling welcome / [GET] endpoint")
        version = Config.API_VERSION
        return jsonify(message=f'Welcome to ToDo rest api {version}')
