from flask import Flask

from src.routes.todo_routes import ItemsRoutes
from src.routes.welcome_routes import WelcomeRoutes


def routes(app: Flask):
    WelcomeRoutes(app)
    ItemsRoutes(app)
